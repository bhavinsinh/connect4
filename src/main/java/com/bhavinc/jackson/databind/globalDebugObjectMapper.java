package com.bhavinc.jackson.databind;

import com.fasterxml.jackson.databind.SerializationFeature;

public class globalDebugObjectMapper extends globalObjectMapper {

	public globalDebugObjectMapper() {
		super();
		this.enable(SerializationFeature.INDENT_OUTPUT);
	}
}
