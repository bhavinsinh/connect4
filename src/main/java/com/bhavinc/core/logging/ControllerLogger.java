package com.bhavinc.core.logging;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class ControllerLogger {

    public static final ThreadLocal logger = new ThreadLocal(){
        @Override
        protected Map<String,String> initialValue() {
            return new HashMap<>();
        }
    };

    public static ThreadLocal<Map<String, String>> getLogger() {
        return logger;
    }

    public static final ThreadLocal<DecimalFormat> decimalFormatter = new ThreadLocal<DecimalFormat>(){
        @Override
        protected DecimalFormat initialValue() {
          return new DecimalFormat("#.##");
        }
    };
}
