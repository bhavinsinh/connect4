package com.bhavinc.core.exceptions;

import lombok.Getter;
import lombok.Setter;

public class BadRequestException extends RuntimeException {

    @Getter @Setter
    private String message;
    @Getter @Setter private int httpStatusCode;

    public BadRequestException() {
        super();
        message = "";
        this.httpStatusCode = 400;
    }

    public BadRequestException(String message) {
        super(message);
        this.message = message;
        this.httpStatusCode = 400;

    }

    public String toString(){
        return message;
    }

}
