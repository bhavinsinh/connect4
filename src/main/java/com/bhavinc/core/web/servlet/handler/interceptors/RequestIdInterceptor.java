package com.bhavinc.core.web.servlet.handler.interceptors;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class RequestIdInterceptor extends HandlerInterceptorAdapter {
	
	public static final String ATTRIBUTE_NAME = "com.bhavinc.request-id";
	public static final String CONTEXT_NAME = "requestId";
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
		throws Exception {
		String id = UUID.randomUUID().toString();
		MDC.put(CONTEXT_NAME, id);
		request.setAttribute(ATTRIBUTE_NAME, id);
		return true;
	}
	
	@Override
	public void afterCompletion(
			HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		MDC.clear();
	}

}
