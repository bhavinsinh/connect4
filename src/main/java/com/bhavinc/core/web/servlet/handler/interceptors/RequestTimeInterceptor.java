package com.bhavinc.core.web.servlet.handler.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.bhavinc.api.utils.Utility;

public class RequestTimeInterceptor extends HandlerInterceptorAdapter {
	
	private static final String ATTRIBUTE_NAME = "com.bhavinc.start-time";
	
	private static final Logger logger = LoggerFactory.getLogger(RequestTimeInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
		throws Exception {
		request.setAttribute(ATTRIBUTE_NAME, System.currentTimeMillis());
		return true;
	}
	
	@Override
	public void afterCompletion(
			HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		long endTime = System.currentTimeMillis();
		long startTime = (Long) request.getAttribute(ATTRIBUTE_NAME);
		long processingTime = endTime - startTime;
		String requestId = (String) request.getAttribute(RequestIdInterceptor.ATTRIBUTE_NAME);
		logger.info("Request completed. RequestId: " + requestId +  "apiRequested:" + Utility.getRequestPath() + " processingTime: " + processingTime + "ms");
	}
}
