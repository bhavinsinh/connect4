package com.bhavinc.api.utils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.bhavinc.api.input.Headers;
import com.bhavinc.api.security.SessionInfo;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.validator.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Utility {

    private static final Logger logger = LoggerFactory
            .getLogger(Utility.class);


    public static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * php equivalent of preg_match
     *
     * @param pattern
     * @param content
     * @return
     */
    public static boolean pregMatch(String pattern, String content) {
        return content.matches(pattern);
    }

    public static String getValidString(String string) {
        return string.replace("\"", "").trim();
    }

    public static boolean isValidMobileNumber(String mobile) {
        if (mobile == null || !(mobile.length() == 10))
            return false;
        else return pregMatch("^[1-9][0-9]{9}", mobile);
    }

    public static boolean isValidEmail(String email) {
        if (email == null)
            return false;
        EmailValidator validator = EmailValidator.getInstance();
        if (validator.isValid(email)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Php Equivalent of json_encode
     *
     * @param o
     * @return
     */
    public static String jsonEncode(Object o) throws JsonProcessingException {
        if (o == null)
            return "";
        return objectMapper.writeValueAsString(o);
    }




    public static Object jsonDecode(String s, Class<?> c) {
        Object u = null;
        if(s != null && !s.equals("")) {
            try {
                u = objectMapper.reader(c).readValue(s);
            } catch (Exception e) {
                logger.error("error while converting json to class", e);
            }
        }
        return u;
    }

    public static boolean isEmpty(List l){
        if(l == null || l.size() == 0)
            return true;
        else return false;
    }

    public static HttpServletRequest getCurrentRequest() {
        final ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
                .currentRequestAttributes();
        final HttpServletRequest request = attr.getRequest();
        return request;
    }

    public static Headers getHeaderForCurrentRequest(){
        final HttpServletRequest request = getCurrentRequest();
        return getHeaderForRequest(request);
    }

    public static Headers getHeaderForRequest(HttpServletRequest request){
        Headers headers = new Headers();
        String user_agent = request.getHeader("user-agent");
        UserAgent userAgent;
        if(user_agent!=null && (user_agent.equals("Swiggy-iOS") || user_agent.equals("Swiggy-Android"))){}
        else{user_agent = "web";}
        if(user_agent.equals("web"))
            userAgent = UserAgent.WEB;
        else if(user_agent.equals("Swiggy-iOS"))
            userAgent = UserAgent.IOS;
        else userAgent = UserAgent.ANDROID;
        headers.setUserAgent(userAgent);
        headers.setVersionCode(getInt(request.getHeader("version-code")));
        headers.setOsVersion(request.getHeader("os-version"));
        headers.setDeviceId(request.getHeader("deviceId"));
        headers.setAppVersion(request.getHeader("app-version"));
        return headers;
    }


    public static HashMap<String, Object> jsonToMap(String jsonString) throws IOException {
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);

        TypeReference<HashMap<String, Object>> typeRef
                = new TypeReference<HashMap<String, Object>>() {
        };

        return mapper.readValue(jsonString, typeRef);
    }


    public static Integer getInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return 0;
        }
    }

    public static SessionInfo getCurrentSession(){
        return (SessionInfo) getCurrentRequest().getAttribute("sessionData");
    }

    public static String getRequestPath() {
        HttpServletRequest request = getCurrentRequest();
        String context = request.getContextPath();
        String fullURL = request.getRequestURI();
        String path = fullURL.substring(fullURL.indexOf(context) + context.length());
        return path;
    }

    public static <T> T convertValue(Object o, Class<T> clazz) {
        return objectMapper.convertValue(o, clazz);
    }

    public static String getCustomerId() {
        SessionInfo sessionData = (SessionInfo) getCurrentRequest().getAttribute("sessionData");
        if (sessionData == null || sessionData.getUser() == null || sessionData.getUser().getUserId() == 0) {
            return null;
        }
        return String.valueOf(sessionData.getUser().getUserId());
    }

    private static String getDateOrdinal(LocalDateTime dateTime) {
        int date = dateTime.getDayOfMonth();
        String dateEnd = "th";
        if (date == 1 || date == 21 || date == 31) {
            dateEnd = "st";
        } else if (date == 2 || date == 22) {
            dateEnd = "nd";
        } else if (date == 3 || date == 23) {
            dateEnd = "rd";
        }
        return dateEnd;
    }


    public static boolean containsKey(Set t,Object k){
        if(t == null)
            return false;
        if(t.contains(k))
            return true;
        else return false;
    }
}
