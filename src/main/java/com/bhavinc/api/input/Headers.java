package com.bhavinc.api.input;

import com.bhavinc.api.utils.UserAgent;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Headers {
    private String osVersion;
    private int versionCode;
    private String deviceId;
    private String appVersion;
    private UserAgent userAgent;
}
