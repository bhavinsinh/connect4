package com.bhavinc.api.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginData {
    @Getter @Setter private String mobile;
    @Getter @Setter private String email;
    @Getter @Setter private String password;
}
