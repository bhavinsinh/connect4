package com.bhavinc.api.helper;


public interface PasswordHashHelper {
     String encode64(byte[] src, int count);
     String cryptPrivate(String password, String setting);
     String gensaltPrivate(byte[] input);
     byte[] stringToUtf8(String string);
     String HashPassword(String password);
     boolean CheckPassword(String password, String storedHash);
}
