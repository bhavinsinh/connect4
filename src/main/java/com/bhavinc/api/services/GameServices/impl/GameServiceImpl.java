package com.bhavinc.api.services.GameServices.impl;

import com.bhavinc.api.security.SessionInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.data.entities.*;
import com.bhavinc.api.pojo.*;
import com.bhavinc.api.services.GameServices.*;
import com.bhavinc.api.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.Objects;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public class GameServiceImpl implements GameService {
    @Autowired
    GameEntityService gameEntityService;
    @Autowired
    GameStateEntityService gameStateEntityService;
    @Autowired
    UserService userService;
    @Autowired
    UserScoreService userScoreService;

    private static final long scorePerGame = 5;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public BaseResponse createGame(GameInput gameInput) {
        GamesEntity gamesEntity = new GamesEntity();
        UsersEntity usersEntity1 = userService.findById(gameInput.getChallengerId());
        UsersEntity usersEntity2 = userService.findById(gameInput.getChallengedId());
        if (usersEntity1 == null) {
            return new BaseResponse(1, "User does not exist.", null);
        }
        if (usersEntity2 == null || usersEntity2.getUserState() != UserState.logged_in) {
            return new BaseResponse(2, "Challenged Player not active.", null);
        }
        GameOutput gameOutput = new GameOutput();
        gamesEntity.setChallenged(gameInput.getChallengedId());
        gamesEntity.setChallenger(gameInput.getChallengerId());
        gamesEntity.setCreated_on(new Date(System.currentTimeMillis()));
        gameEntityService.saveOrUpdate(gamesEntity);
        usersEntity1.setUserState(UserState.challenger);
        usersEntity2.setUserState(UserState.challenged);
        userService.saveOrUpdate(usersEntity1);
        userService.saveOrUpdate(usersEntity2);
        gameOutput.setGameId(gamesEntity.getId());
        return new BaseResponse(0, "success", gameOutput);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public BaseResponse accept(GameInput gameInput) throws JsonProcessingException {
        GamesEntity gamesEntity = gameEntityService.findById(gameInput.getGameId());
        if (gamesEntity.getChallenged() != gameInput.getChallengedId()) {
            return new BaseResponse(1, "Unauthorized action", null);
        }
        if (gamesEntity.getState() == GameStateEnum.active) {
            return new BaseResponse(1, "Already accepted", null);
        }
        gamesEntity.setState(GameStateEnum.active);
        gameEntityService.saveOrUpdate(gamesEntity);
        UsersEntity usersEntity1 = userService.findById(gamesEntity.getChallenger());
        UsersEntity usersEntity2 = userService.findById(gamesEntity.getChallenged());
        usersEntity1.setUserState(UserState.playing);
        usersEntity2.setUserState(UserState.playing);
        userService.saveOrUpdate(usersEntity1);
        userService.saveOrUpdate(usersEntity2);
        GamesStateEntity gamesStateEntity = new GamesStateEntity();
        gamesStateEntity.setGameId(gamesEntity.getId());
        gamesStateEntity.setPlayer1(usersEntity1.getId());
        gamesStateEntity.setPlayer2(usersEntity2.getId());
        gamesStateEntity.setLastMoveBy(usersEntity2.getId());
        gamesStateEntity.setGameState(Utility.jsonEncode(new GameState()));
        gameStateEntityService.saveOrUpdate(gamesStateEntity);
        return new BaseResponse(0, "success", null);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public BaseResponse getUsers(boolean isActive) {
        return new BaseResponse(0, "success", userService.getUsers(isActive));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public BaseResponse getCurrentGameState(Long gameId) {
        return new BaseResponse(0, "success",
                Utility.jsonDecode(gameStateEntityService.getCurrentGameState(gameId).getGameState(), GameState.class));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public BaseResponse playMove(GameMove gameMove, SessionInfo sessionInfo) {
        GamesStateEntity gamesStateEntity = gameStateEntityService.getCurrentGameState(gameMove.getGameId());
        GamesEntity gamesEntity = gameEntityService.findById(gameMove.getGameId());
        if (gamesEntity == null || gamesStateEntity == null) {
            return new BaseResponse(1, "Game not started", null);
        }
        if (gamesEntity.getChallenged() != gameMove.getUserID() || gamesEntity.getChallenger() != gameMove.getUserID()) {
            return new BaseResponse(1, "Unauthorized action", null);
        }
        if (gamesEntity.getState() != GameStateEnum.active) {
            return new BaseResponse(1, "Game over", gamesStateEntity);
        }
        if (gamesStateEntity.getLastMoveBy() == gameMove.getUserID()) {
            return new BaseResponse(2, "Please wait for your turn!", gamesStateEntity);
        }
        if (gamesStateEntity.getPlayer1() != gameMove.getUserID() && gamesStateEntity.getPlayer2() != gameMove.getUserID()) {
            return new BaseResponse(3, "Unauthorized action!", null);
        }
        GameState gameState = (GameState) Utility.jsonDecode(gamesStateEntity.getGameState(), GameState.class);
        if (gameState.getMatrix() == null) {
            return new BaseResponse(1, "Unknown Error", null);
        }
        long[][] matrix = gameState.getMatrix();
        int row = gameMove.getRow();
        int col = gameMove.getCol();
        if (row < 0 || row > 5 || col < 0 || col > 6) {
            return new BaseResponse(4, "Invalid Move", null);
        }
        if (matrix[row][col] != 0) {
            return new BaseResponse(4, "Invalid Move", null);
        }

        matrix[row][col] = gameMove.getUserID();
        gamesStateEntity.setLastMoveBy(gameMove.getUserID());
        try {
            gamesStateEntity.setGameState(Utility.jsonEncode(gameState));
            gameStateEntityService.saveOrUpdate(gamesStateEntity);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        GameResult gameResult = checkIfGameOver(gameMove, matrix, gamesStateEntity.getPlayer1(), gamesStateEntity.getPlayer2());
        if (gameResult.isGameOver()) {
            UsersEntity usersEntity1 = userService.getUser(gamesStateEntity.getPlayer1());
            UsersEntity usersEntity2 = userService.getUser(gamesStateEntity.getPlayer2());
            UsersScoresEntity usersScoresEntity1 = userScoreService.findByUserId(gamesStateEntity.getPlayer1());
            UsersScoresEntity usersScoresEntity2 = userScoreService.findByUserId(gamesStateEntity.getPlayer2());
            if (usersScoresEntity1 == null) {
                usersScoresEntity1 = new UsersScoresEntity();
                usersScoresEntity1.setUserId(gamesStateEntity.getPlayer1());
            }
            if (usersScoresEntity2 == null) {
                usersScoresEntity2 = new UsersScoresEntity();
                usersScoresEntity2.setUserId(gamesStateEntity.getPlayer2());
            }
            gamesEntity.setWinner(gameResult.getWinner());
            if (Objects.equals(usersScoresEntity1.getUserId(), gameResult.getWinner())) {
                usersScoresEntity1.setUserScore(usersScoresEntity1.getUserScore() + scorePerGame);

            } else if (Objects.equals(usersScoresEntity2.getUserId(), gameResult.getWinner())) {
                usersScoresEntity2.setUserScore(usersScoresEntity2.getUserScore() + scorePerGame);
            }
            userScoreService.saveOrUpdate(usersScoresEntity1);
            userScoreService.saveOrUpdate(usersScoresEntity2);
            usersEntity1.setUserState(UserState.logged_in);
            usersEntity2.setUserState(UserState.logged_in);
            userService.saveOrUpdate(usersEntity1);
            userService.saveOrUpdate(usersEntity2);
            gamesEntity.setState(GameStateEnum.finished);
            gameEntityService.saveOrUpdate(gamesEntity);
        }
        return new BaseResponse(0, "Success", gameResult);
    }

    private GameResult checkIfGameOver(GameMove gameMove, long[][] matrix, long player1, long player2) {
        GameResult gameResult = new GameResult();
        int x = gameMove.getRow();
        int y = gameMove.getCol();
        long r = matrix[x][y];
        if (((x >= 3 && r == matrix[x - 1][y]) && (r == matrix[x - 2][y]) && (r == matrix[x - 3][y]))
                || (x + 3 <= 5 && r == matrix[x + 1][y] && r == matrix[x + 2][y] && r == matrix[x + 3][y])
                || (y >= 3 && r == matrix[x][y - 1] && r == matrix[x][y - 2] && r == matrix[x][y - 3])
                || (y + 3 <= 6 && r == matrix[x][y + 1] && r == matrix[x][y + 2] && r == matrix[x][y + 3])
                ) {
            gameResult.setWinner(r);
            gameResult.setGameOver(true);
        }
        return gameResult;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public BaseResponse getUserStatus(Long userId) {
        GamesEntity gamesEntity = gameEntityService.getActiveGame(userId);
        if (gamesEntity == null) {
            return new BaseResponse(1, "No challlenge", null);
        }
        return new BaseResponse(0, "Challenged!", gamesEntity);
    }

}
