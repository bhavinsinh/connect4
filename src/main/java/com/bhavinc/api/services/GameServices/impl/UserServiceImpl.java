package com.bhavinc.api.services.GameServices.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.LoginConstraintViolationException;
import com.bhavinc.api.data.entities.UserState;
import com.bhavinc.api.data.entities.UsersEntity;
import com.bhavinc.api.data.model.dao.TokenUtils;
import com.bhavinc.api.data.model.dao.UserDao;
import com.bhavinc.api.errors.BaseError;
import com.bhavinc.api.helper.PasswordHashHelper;
import com.bhavinc.api.helper.impl.PasswordHashImpl;
import com.bhavinc.api.input.LoginData;
import com.bhavinc.api.pojo.*;
import com.bhavinc.api.security.SessionInfo;
import com.bhavinc.api.services.GameServices.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public class UserServiceImpl extends BaseEntityServiceImpl<UsersEntity, Long, UserDao>
        implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public UserDao getDao() {
        return userDao;
    }

    @Override
    public List<UsersEntity> getUsers(boolean isActive) {
        return userDao.getUsersEntity(isActive);
    }

    @Override
    public UsersEntity getUser(Long userId) {
        return userDao.findById(userId);
    }

    @Autowired
    TokenUtils tokenUtils;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Map<String, Object> newSignup(SignUp newSignup, SessionInfo sInfo) {
        UsersEntity usersEntityExisting = userDao.findByEmail(newSignup.getEmail());
        HashMap hashMap =  (new HashMap<>());
        if (usersEntityExisting != null) {
            hashMap.put("error", new BaseError(1, "User Already exists"));
        }
        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername(newSignup.getName());
        usersEntity.setUserNiceName(newSignup.getName());
        usersEntity.setMobile(newSignup.getMobile());
        usersEntity.setEmail(newSignup.getEmail());
        PasswordHashHelper pwd_hash = new PasswordHashImpl(8);
        String hashed_pwd = pwd_hash.HashPassword(newSignup.getPassword());
        usersEntity.setPassword(hashed_pwd);
        usersEntity.setCreated_on(new Date(System.currentTimeMillis()));
        usersEntity.setUserState(UserState.logged_in);
        userDao.saveOrUpdate(usersEntity);
        hashMap.put("user", usersEntity);
        return hashMap;
    }

    @Override
    public BaseResponse loginViaPassword(LoginData data, SessionInfo sessionData) throws JsonProcessingException, LoginConstraintViolationException {
        UsersEntity userEntity = userDao.findByEmail(data.getEmail());
        if(userEntity == null) {
            return new ErrorResponse("User with this mobile number is not registered", null);
        }

        PasswordHashHelper pwd_hash = new PasswordHashImpl(8);
        boolean pwd_match = pwd_hash.CheckPassword(data.getPassword(), userEntity.getPassword());

        if(pwd_match){
            User user = new User(" ", userEntity);
            sessionData.setUser(user);
            sessionData.setUpdate(true);
            return verified(sessionData);
        }
        else{
            return new ErrorResponse("Invalid Password", null);
        }
    }

    public BaseResponse verified(SessionInfo sessionData) throws JsonProcessingException, LoginConstraintViolationException {
        User userData = sessionData.getUser();
        Map<String, Object> userDetailsMap = new HashMap<>();
        tokenUtils.removeToken(sessionData.getId());
        userData.setVerified(true);
        String token = tokenUtils.generate64Token();
        sessionData.getUser().setToken(token);
        sessionData.setUpdate(true);
        String tid = tokenUtils.getToken(sessionData, (long) 10080 * 60);
        userDetailsMap.put("tid", tid);
        userDetailsMap.put("userId", userData.getUserId());
        userDetailsMap.put("mobile", userData.getMobile());
        userDetailsMap.put("email", userData.getEmail());
        userDetailsMap.put("token", sessionData.getUser().getToken());
        BaseResponse response = new SuccessResponse(userDetailsMap);
        sessionData.setId(tid);
        response.updateTid(tid);
        return response;
    }
}
