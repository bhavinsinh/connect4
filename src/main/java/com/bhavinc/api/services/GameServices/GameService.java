package com.bhavinc.api.services.GameServices;

import com.bhavinc.api.security.SessionInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.pojo.BaseResponse;
import com.bhavinc.api.pojo.GameInput;
import com.bhavinc.api.pojo.GameMove;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public interface GameService {
    public BaseResponse createGame(GameInput gameInput);
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    BaseResponse accept(GameInput gameInput) throws JsonProcessingException;
    public BaseResponse getUsers(boolean isActive);

    BaseResponse getCurrentGameState(Long gameId);
    BaseResponse playMove(GameMove gameMove, SessionInfo sessionInfo);

    BaseResponse getUserStatus(Long userId);
}
