package com.bhavinc.api.services.GameServices.impl;

import com.bhavinc.api.data.entities.GamesEntity;
import com.bhavinc.api.data.model.dao.GameDao;
import com.bhavinc.api.services.GameServices.GameEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public class GameEntityServiceImpl extends BaseEntityServiceImpl<GamesEntity, Long, GameDao>
        implements GameEntityService {

    @Autowired
    GameDao gameDao;
    @Override
    public GameDao getDao() {
        return gameDao;
    }

    @Override
    public GamesEntity getActiveGame(Long userID) {
        return gameDao.getActiveGameByUserId(userID);
    }
    @Override
    public GamesEntity getGameByUserId(Long userID) {
        return gameDao.getByUserId(userID);
    }
}
