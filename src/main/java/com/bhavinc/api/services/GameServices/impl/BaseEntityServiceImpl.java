package com.bhavinc.api.services.GameServices.impl;

import com.bhavinc.api.data.entities.BaseEntity;
import com.bhavinc.api.data.model.dao.BaseEntityDao;
import com.bhavinc.api.services.GameServices.BaseEntityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by bhavi on 06-06-2016.
 */

@Transactional(propagation= Propagation.REQUIRED)
@Service
public abstract class BaseEntityServiceImpl<T extends BaseEntity<ID>, ID extends Serializable, DAO extends BaseEntityDao<T, ID>>
        implements BaseEntityService<T,ID> {

    public abstract DAO getDao();

    @Override
    public T findById(ID id) {
        return getDao().findById(id);
    }

    @Override
    public void saveOrUpdate(T entity) {
        getDao().saveOrUpdate(entity);
    }

    @Override
    public void saveOrUpdateAll(Collection<T> entity) {
        getDao().saveOrUpdateAll(entity);
    }

    @Override
    public void delete(T entity) {
        getDao().delete(entity);
    }

    @Override
    public void deleteAll(List<T> entities) {
        getDao().deleteAll(entities);
    }

}
