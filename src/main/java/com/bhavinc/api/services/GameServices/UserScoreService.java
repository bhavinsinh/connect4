package com.bhavinc.api.services.GameServices;

import com.bhavinc.api.data.entities.UsersScoresEntity;
import org.springframework.stereotype.Service;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public interface UserScoreService extends BaseEntityService<UsersScoresEntity, Long> {
    public UsersScoresEntity findByUserId(Long userID);
}
