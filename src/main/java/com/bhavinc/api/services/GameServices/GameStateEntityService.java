package com.bhavinc.api.services.GameServices;

import com.bhavinc.api.data.entities.GamesStateEntity;
import org.springframework.stereotype.Service;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public interface GameStateEntityService extends BaseEntityService<GamesStateEntity, Long> {
    public GamesStateEntity getCurrentGameState(Long gameId);
}
