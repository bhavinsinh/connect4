package com.bhavinc.api.services.GameServices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.LoginConstraintViolationException;
import com.bhavinc.api.data.entities.UsersEntity;
import com.bhavinc.api.input.LoginData;
import com.bhavinc.api.pojo.BaseResponse;
import com.bhavinc.api.pojo.SignUp;
import com.bhavinc.api.security.SessionInfo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public interface UserService extends BaseEntityService<UsersEntity, Long> {
    public List<UsersEntity> getUsers(boolean isActive);
    UsersEntity getUser(Long userId);

    Map<String,Object> newSignup(SignUp newSignup, SessionInfo sInfo);

    BaseResponse loginViaPassword(LoginData data, SessionInfo sessionData) throws JsonProcessingException, LoginConstraintViolationException;
}
