package com.bhavinc.api.services.GameServices;

import com.bhavinc.api.data.entities.GamesEntity;
import org.springframework.stereotype.Service;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public interface GameEntityService extends BaseEntityService<GamesEntity, Long> {
    public GamesEntity getActiveGame(Long userID);

    GamesEntity getGameByUserId(Long userID);
}
