package com.bhavinc.api.services.GameServices.impl;

import com.bhavinc.api.data.entities.GamesStateEntity;
import com.bhavinc.api.data.model.dao.GameStateDao;
import com.bhavinc.api.services.GameServices.GameStateEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public class GameStateServiceImpl extends BaseEntityServiceImpl<GamesStateEntity, Long, GameStateDao>
        implements GameStateEntityService {

    @Autowired
    GameStateDao gameStateDao;

    @Override
    public GameStateDao getDao() {
        return gameStateDao;
    }

    @Override
    public GamesStateEntity getCurrentGameState(Long gameId) {
        return gameStateDao.getLastStateByGameId(gameId);
    }
}

