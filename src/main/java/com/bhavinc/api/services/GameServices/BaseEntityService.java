package com.bhavinc.api.services.GameServices;

import com.bhavinc.api.data.entities.BaseEntity;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public interface BaseEntityService <T extends BaseEntity<ID>, ID extends Serializable>{
    public T findById(ID id);
    public void saveOrUpdate(T entity);
    public void saveOrUpdateAll(Collection<T> entity);
    public void delete(T entity);
    public void deleteAll(List<T> entities);
}
