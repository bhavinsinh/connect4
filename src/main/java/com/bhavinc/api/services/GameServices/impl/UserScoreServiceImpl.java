package com.bhavinc.api.services.GameServices.impl;

import com.bhavinc.api.data.entities.UsersScoresEntity;
import com.bhavinc.api.data.model.dao.UserScoreDao;
import com.bhavinc.api.services.GameServices.UserScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by bhavi on 06-06-2016.
 */
@Service
public class UserScoreServiceImpl extends BaseEntityServiceImpl<UsersScoresEntity, Long, UserScoreDao>
        implements UserScoreService {

    @Autowired
    UserScoreDao userScoreDao;

    @Override
    public UserScoreDao getDao() {
        return userScoreDao;
    }

    @Override
    public UsersScoresEntity findByUserId(Long userID) {
        return userScoreDao.findByUserId(userID);
    }
}