package com.bhavinc.api.services.AuxilillaryServices;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.bhavinc.api.data.model.dao.impl.TypeReference;
import org.springframework.stereotype.Service;

/**
 * Created by bhavi on 09-06-2016.
 */
@Service
public class CachingService<K> {
    private static final String host = "127.0.0.1";
    private static final int port = 6379;
    private static final int dbIndex = 0;

    public <T> T get(String key, TypeReference<T> ref){
        RedisClient redisClient = new RedisClient(host, port, dbIndex);
        String value =  redisClient.get(key);
        if( value == null)
            return null;
        redisClient.close();
        return getGson().fromJson(value, ref.getType());
    }

    public K get(String key, Class T){
        RedisClient redisClient = new RedisClient(host, port, dbIndex);
        String value = redisClient.get(key);
        if( value == null)
            return null;

        if(T == String.class){
            return (K)value;
        }
        redisClient.close();
        return (K)  getGson().fromJson(value,T);
    }

    public void set(String key, K obj, long expiry){
        RedisClient redisClient = new RedisClient(host, port, dbIndex);
        String jsonEncodedString;
        if(obj instanceof String) {
            jsonEncodedString = (String) obj;
        } else {
            jsonEncodedString = getGson().toJson(obj);
        }
        if(expiry == -1) {
            redisClient.set(key, jsonEncodedString);
        } else {
            redisClient.set(key, jsonEncodedString, expiry);
        }
        redisClient.close();
    }

    public void delete(String key) {
        RedisClient redisClient = new RedisClient(host, port, dbIndex);
        redisClient.del(key);
        redisClient.close();
    }

    private Gson getGson() {
        return new GsonBuilder().create();
    }

}
