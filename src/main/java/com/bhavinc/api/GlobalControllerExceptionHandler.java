package com.bhavinc.api;

import com.fasterxml.jackson.core.JsonParseException;
import com.bhavinc.api.pojo.ErrorResponse;
import com.bhavinc.api.utils.Utility;
import com.bhavinc.core.exceptions.BadRequestException;
import com.bhavinc.core.web.servlet.handler.interceptors.RequestIdInterceptor;
import org.apache.catalina.connector.ClientAbortException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @Value("${email_onErrorEmailId}") private String errorEmail;
    @Value("${email_context}") private String emailContext;

    private static final Logger logger = LoggerFactory
            .getLogger(GlobalControllerExceptionHandler.class);

    @ExceptionHandler(BadRequestException.class)
    public void exceptionHandler(HttpServletRequest request,
                                 HttpServletResponse response, BadRequestException exception) throws IOException {
    	exception.printStackTrace();
        logger.error("Exception thrown: ", exception);
        response.setStatus(exception.getHttpStatusCode());
        response.getWriter().write(exception.getMessage());
    }

    @ExceptionHandler(ClientAbortException.class)
    public void  clientAbortExceptionHandler(HttpServletRequest request,
                                 HttpServletResponse response, ClientAbortException exception) throws IOException {
        exception.printStackTrace();
        logger.error("Exception thrown: ", exception);
        response.setStatus(500);
        response.getWriter().write(exception.getMessage());
    }

    @ExceptionHandler(LoginConstraintViolationException.class)
    public void  loginConstrainViolationExceptionHandler(HttpServletRequest request,
                                             HttpServletResponse response, LoginConstraintViolationException exception) throws IOException {
        logger.error("Login Constraint Violation Exception thrown: " + exception.getMessage());
        response.setStatus(500);
        response.getWriter().write(Utility.jsonEncode(new ErrorResponse(500, "Duplicate Login Request.")));
    }

    @ExceptionHandler(JsonParseException.class)
    public void jsonParseExceptionHandler(HttpServletRequest request,
                                          HttpServletResponse response, JsonParseException exception) throws IOException {
    	exception.printStackTrace();
    	logger.error("Exception thrown: ", exception);
        response.setStatus(400);
        response.getWriter().write(exception.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public void httpMessageExceptionHandler(HttpServletRequest request,
                                            HttpServletResponse response, HttpMessageNotReadableException exception) throws IOException {
    	exception.printStackTrace();
    	logger.error("Exception thrown: ", exception);
        response.setStatus(500);
        response.getWriter().write("Http message is not readable : " + exception.getMessage());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public void httpRequestNotFoundExceptionHandler(HttpServletRequest request,
                                            HttpServletResponse response, HttpRequestMethodNotSupportedException exception) throws IOException {
    	exception.printStackTrace();
    	logger.error("Exception thrown: ", exception);
        response.setStatus(500);
        response.getWriter().write(exception.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public void genericExceptionHandler(HttpServletRequest request,
                                        HttpServletResponse response, Exception exception) throws Exception {
    	exception.printStackTrace();
    	logger.error("Exception: ", exception);
        response.setStatus(500);
        response.getWriter().write(Utility.jsonEncode(new ErrorResponse(500, "Internal Server Error")));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public void missingParamsHandler(HttpServletResponse response, Exception exception) throws IOException {
    	exception.printStackTrace();
    	logger.error("Exception:",exception);
        response.setStatus(500);
        response.getWriter().write("Required parameter(s) are not present.");
    }

    @ExceptionHandler(TypeMismatchException.class)
    public void typeMisMatchHandler(HttpServletResponse response, Exception exception) throws IOException {
        logger.error("Exception:",exception);
        response.setStatus(500);
        response.getWriter().write("Type mismatch. Please check parameter values.");
    }
}
