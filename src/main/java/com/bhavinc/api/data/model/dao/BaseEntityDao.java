package com.bhavinc.api.data.model.dao;

import com.bhavinc.api.data.entities.BaseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by bhavi on 06-06-2016.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface BaseEntityDao <T extends BaseEntity, ID extends Serializable> {
    public T findById(ID id);
    public void saveOrUpdate(T entity);
    public void saveOrUpdateAll(Collection<T> entity);
    public void delete(T entity);
    public void deleteAll(List<T> entities);
}
