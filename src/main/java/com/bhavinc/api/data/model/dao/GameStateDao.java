package com.bhavinc.api.data.model.dao;

import com.bhavinc.api.data.entities.GamesStateEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bhavin on 06-06-2016.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface GameStateDao extends BaseEntityDao<GamesStateEntity, Long> {
    public GamesStateEntity getLastStateByGameId(Long id);
}
