package com.bhavinc.api.data.model.dao.impl;

import com.bhavinc.api.data.entities.UserState;
import com.bhavinc.api.data.entities.UsersEntity;
import com.bhavinc.api.data.model.dao.UserDao;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by bhavi on 06-06-2016.
 */
@Repository
public class UserDaoImpl extends BaseEntityDaoImpl<UsersEntity, Long> implements UserDao {
    public UserDaoImpl() {
        super(UsersEntity.class);
    }

    @Override
    public List<UsersEntity> getUsersEntity(boolean isActive) {
        if (isActive) {
            return getSession().createCriteria(persistentClass)
                    .add(Restrictions.eq("userState", UserState.logged_in))
                    .list();
        }
        return getSession().createCriteria(persistentClass)
                .add(Restrictions.ne("userState", UserState.logged_in))
                .list();
    }

    @Override
    public UsersEntity findByEmail(String email) {
        return (UsersEntity) getSession().createCriteria(persistentClass)
                .add(Restrictions.eq("email", email))
                .uniqueResult();
    }
}
