package com.bhavinc.api.data.model.dao;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.errors.BaseError;
import com.bhavinc.api.security.SessionInfo;


public interface TokenUtils {
    String getToken(String info);
    public String getToken(SessionInfo info, Long expirationTime) throws JsonProcessingException;
     Map<String,Object> validate(String token,boolean isVerifiedCheckNeeded,HttpServletRequest request);
    String getSession(String token);
    SessionInfo removeToken(String token);
    String generate64Token();
    void linkMobileNumber(String mobile,String tid,long timeout);
    void update(String token,SessionInfo info) throws JsonProcessingException;
     BaseError getError(Map<String,Object> res);
    boolean isTokenValid(Map<String,Object> res);
}
