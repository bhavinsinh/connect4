package com.bhavinc.api.data.model.dao.impl;

import com.bhavinc.api.data.entities.BaseEntity;
import com.bhavinc.api.data.model.dao.BaseEntityDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by bhavi on 06-06-2016.
 */
@Transactional(propagation= Propagation.MANDATORY)
public abstract class BaseEntityDaoImpl<T extends BaseEntity, ID extends Serializable> implements BaseEntityDao<T, ID> {

    @Autowired
    SessionFactory sessionFactory;

    Class<T> persistentClass;

    public BaseEntityDaoImpl(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    public Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    @Override
    public T findById(ID id) {
        return (T) getSession().get(persistentClass, id);
    }

    @Override
    public void saveOrUpdate(T entity) {
        try{
            getSession().saveOrUpdate(entity.getTableName(), entity);
        } finally{
        }
    }

    @Override
    public void saveOrUpdateAll(Collection<T> entitiesCollection) {
        entitiesCollection.forEach(this::saveOrUpdate);
    }

    @Override
    public void delete(T entity) {
        getSession().delete(entity);
    }

    @Override
    public void deleteAll(List<T> entities) {
        for (T entity:entities) {
            delete(entity);
        }
    }

}
