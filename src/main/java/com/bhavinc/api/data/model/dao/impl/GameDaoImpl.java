package com.bhavinc.api.data.model.dao.impl;

import com.bhavinc.api.data.entities.GameStateEnum;
import com.bhavinc.api.data.entities.GamesEntity;
import com.bhavinc.api.data.model.dao.GameDao;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Created by bhavi on 06-06-2016.
 */
@Repository
public class GameDaoImpl extends BaseEntityDaoImpl<GamesEntity, Long> implements GameDao {
    public GameDaoImpl() {
        super(GamesEntity.class);
    }

    @Override
    public GamesEntity getByUserId(Long userId) {
        return (GamesEntity) getSession().createCriteria(persistentClass)
                .add(Restrictions.eq("challenged", userId)
                ).uniqueResult();
    }

    @Override
    public GamesEntity getActiveGameByUserId(Long userId) {
        return (GamesEntity) getSession().createCriteria(persistentClass)
                .add(Restrictions.conjunction().
                        add(Restrictions.eq("state", GameStateEnum.inactive))
                        .add(Restrictions.eq("challenged", userId))
                ).uniqueResult();
    }
}
