package com.bhavinc.api.data.model.dao;

import com.bhavinc.api.data.entities.UsersEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by bhavi on 06-06-2016.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface UserDao extends BaseEntityDao<UsersEntity, Long>{
    public List<UsersEntity> getUsersEntity(boolean isActive);
    public UsersEntity findByEmail(String email);
}