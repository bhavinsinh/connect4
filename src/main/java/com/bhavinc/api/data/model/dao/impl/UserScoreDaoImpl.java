package com.bhavinc.api.data.model.dao.impl;

import com.bhavinc.api.data.entities.UsersScoresEntity;
import com.bhavinc.api.data.model.dao.UserScoreDao;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Created by bhavi on 06-06-2016.
 */
@Repository
public class UserScoreDaoImpl extends BaseEntityDaoImpl<UsersScoresEntity, Long> implements UserScoreDao {
    public UserScoreDaoImpl() {
        super(UsersScoresEntity.class);
    }

    @Override
    public UsersScoresEntity findByUserId(Long userId) {
        return (UsersScoresEntity) getSession().createCriteria(persistentClass)
                .add(Restrictions.eq("userId", userId))
                .uniqueResult();
    }
}
