package com.bhavinc.api.data.model.dao.impl;

import com.bhavinc.api.data.entities.GamesStateEntity;
import com.bhavinc.api.data.model.dao.GameStateDao;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by bhavi on 06-06-2016.
 */
@Repository
public class GameStateDaoImpl extends BaseEntityDaoImpl<GamesStateEntity, Long> implements GameStateDao {
    public GameStateDaoImpl() {
        super(GamesStateEntity.class);
    }

    @Override
    public GamesStateEntity getLastStateByGameId(Long id) {
        List<GamesStateEntity> gamesStateEntityList = (List<GamesStateEntity>) getSession().createCriteria(persistentClass)
                .add(Restrictions.eq("gameId", id))
                .addOrder(Order.desc("updatedOn"))
                .list();
        if (gamesStateEntityList == null) return null;
        return gamesStateEntityList.get(0);
    }
}
