package com.bhavinc.api.data.model.dao.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.bhavinc.api.services.AuxilillaryServices.CachingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.data.model.bo.ErrorInfo;
import com.bhavinc.api.data.model.dao.TokenUtils;
import com.bhavinc.api.errors.BaseError;
import com.bhavinc.api.security.SessionInfo;
import com.bhavinc.api.utils.Utility;


@Service
public class TokenUtilsImpl implements TokenUtils {


    private final static String SESSION_KEY = ""; //todo change, currently in this format to support php api
    private static final Logger logger = LoggerFactory.getLogger(TokenUtilsImpl.class);

    @Autowired
    private CachingService<String> stringTemplate;

    private String getSessionKey(String token) {
        return SESSION_KEY + token;
    }

    @Override
    public String getToken(String info) {
        String token = UUID.randomUUID().toString();
        stringTemplate.set(getSessionKey(token), info, 60 * 60);
        return token;
    }

    public String getToken(SessionInfo info, Long expirationTime) throws JsonProcessingException {
        String token = UUID.randomUUID().toString();
        info.setId(token);
        stringTemplate.set(getSessionKey(token), Utility.jsonEncode(info), expirationTime);
        return token;
    }

    @Override
    public SessionInfo removeToken(String token) {
        String res = getSession(token);
        stringTemplate.delete(getSessionKey(token));
        return (SessionInfo) Utility.jsonDecode(res, SessionInfo.class);
    }

    @Override
    public void update(String token, SessionInfo info) throws JsonProcessingException {
        if (info != null && info.getUser() != null && info.getUser().isVerified()) {
            stringTemplate.set(getSessionKey(token), Utility.jsonEncode(info), getLoggedinTimeOut());
        } else {
            stringTemplate.set(getSessionKey(token), Utility.jsonEncode(info), getNonLoggedInTimeout());
        }
    }

    @Override
    public String generate64Token() {
        return UUID.randomUUID().toString() + UUID.randomUUID().toString();
    }

    @Override
    public Map<String, Object> validate(String tid, boolean isVerifiedCheckNeeded, HttpServletRequest request) {
        Map<String, Object> result = new HashMap<>();
        SessionInfo sessionData = (SessionInfo) Utility.jsonDecode(getSession(tid), SessionInfo.class);
        result.put("sessionData", sessionData);
        if (sessionData == null || sessionData.getUser() == null) {
            return invalidSessionResponse(ErrorInfo.INVALID_OR_EMPTY_SESSION, ErrorInfo.INVALID_SESSION_CODE);
        }

        if (!Utility.isValidMobileNumber(sessionData.getUser().getMobile())) {
            return invalidSessionResponse(ErrorInfo.INVALID_PHONE_NO, ErrorInfo.INVALID_SESSION_CODE);
        }

        if (!Utility.isValidEmail(sessionData.getUser().getEmail())) {
            return invalidSessionResponse(ErrorInfo.INVALID_EMAIL_ID, ErrorInfo.INVALID_SESSION_CODE);
        }

        if (sessionData.getUser().getUserId() == 0)
            return invalidSessionResponse(ErrorInfo.INVALID_SESSION_MESSAGE, ErrorInfo.INVALID_SESSION_CODE);

        if (isVerifiedCheckNeeded) {
            if (!sessionData.getUser().isVerified()) {
                return invalidSessionResponse(ErrorInfo.SESSION_NOT_VERIFIED, ErrorInfo.INVALID_SESSION_CODE);
            }
            if (request == null) {
                return invalidSessionResponse(ErrorInfo.EMPTY_REQUEST, ErrorInfo.INVALID_SESSION_CODE);
            }
            String token = request.getHeader("token");


            if (!sessionData.getUser().getToken().equals(token)) {
                return invalidSessionResponse(ErrorInfo.TOKEN_NOT_MATCHED, ErrorInfo.INVALID_SESSION_CODE);
            }
        }
        result.put("isValid", true);
        return result;
    }

    public boolean isTokenValid(Map<String, Object> res) {
        if ((Boolean) res.get("isValid"))
            return true;
        return false;
    }


    public BaseError getError(Map<String, Object> res) {
        if (!isTokenValid(res))
            return (BaseError) res.get("error");
        else
            return null;
    }

    private Map<String, Object> invalidSessionResponse(String message, int code) {
        Map<String, Object> result = new HashMap<>();
        result.put("isValid", false);
        result.put("error", new BaseError(code, message));
        return result;
    }

    @Override
    public void linkMobileNumber(String mobile, String tid, long timeout) {
//        stringTemplate.set(getCallVerifyKey(mobile), tid, timeout);
    }

    @Override
    public String getSession(String token) {
        if (token == null) {
            return null;
        }
        return stringTemplate.get(getSessionKey(token), String.class);
    }

    private long getLoggedinTimeOut() {
        long defaultTimeout = 10080 * 60;
        String timeout = String.valueOf(defaultTimeout);
        return Long.parseLong(timeout) * 60;
    }

    private long getNonLoggedInTimeout() {
        String timeout = "20";
        return Long.parseLong(timeout) * 60;
    }


}
