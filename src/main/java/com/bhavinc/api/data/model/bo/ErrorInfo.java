package com.bhavinc.api.data.model.bo;

/**
 * Created by atulagrawal1 on 23/07/15.
 */
public interface ErrorInfo {
    String errorCode = "error";
    String INVALID_SESSION_MESSAGE="Session expired. Please login again.";
    String INVALID_OR_EMPTY_SESSION="Session expired. Please login again.";
    int INVALID_SESSION_CODE=999;
    String INVALID_PHONE_NO="Invalid mobile number.";
    String INVALID_EMAIL_ID="Invalid email id.";
    String SESSION_NOT_VERIFIED="Session expired. Please login again.";
    String EMPTY_REQUEST="Session expired. Please login again.";
    String TOKEN_NOT_MATCHED="Session expired. Please login again.";
}
