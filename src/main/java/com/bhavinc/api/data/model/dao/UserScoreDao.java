package com.bhavinc.api.data.model.dao;

import com.bhavinc.api.data.entities.UsersScoresEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bhavi on 06-06-2016.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface UserScoreDao extends BaseEntityDao<UsersScoresEntity, Long> {
    public UsersScoresEntity findByUserId(Long userId);
}
