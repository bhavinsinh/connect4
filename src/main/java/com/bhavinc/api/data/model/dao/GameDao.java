package com.bhavinc.api.data.model.dao;

import com.bhavinc.api.data.entities.GamesEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bhavi on 06-06-2016.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface GameDao extends BaseEntityDao<GamesEntity, Long> {
    public GamesEntity getByUserId(Long userId);

    GamesEntity getActiveGameByUserId(Long userId);
}
