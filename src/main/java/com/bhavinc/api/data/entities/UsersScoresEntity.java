package com.bhavinc.api.data.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bhavi on 05-06-2016.
 */
@Entity
@Table(name ="user_scores")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class UsersScoresEntity extends BaseEntity<Long> {
    @Column(name = "id", updatable = false, insertable = true)
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @Column(name = "user_id", insertable = true, updatable = true)
    private Long userId;
    @Column(name = "score", insertable = true, updatable = true)
    private long userScore;
    @Column(name = "ranking", insertable = true, updatable = true)
    private long userRanking;
    @Column(name = "created_on", insertable = true, updatable = false)
    private Date created_on;
    @Column(name = "updated_on", insertable = false, updatable = true)
    private Date updated_on;
}
