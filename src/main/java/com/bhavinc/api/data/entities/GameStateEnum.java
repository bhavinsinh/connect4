package com.bhavinc.api.data.entities;

/**
 * Created by bhavi on 05-06-2016.
 */
public enum GameStateEnum {
    inactive, active, finished, abandoned
}
