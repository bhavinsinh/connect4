package com.bhavinc.api.data.entities;

/**
 * Created by bhavi on 05-06-2016.
 */
public enum UserState {
    logged_in, logged_out, playing, challenged, challenger
}
