package com.bhavinc.api.data.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bhavi on 05-06-2016.
 */


@Entity
@Table(name = "games_state")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class GamesStateEntity extends BaseEntity<Long> {

    @Column(name = "id", nullable = false, updatable = false, insertable = true)
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @Column(name = "game_id", nullable = false, updatable = false, insertable = true)
    private Long gameId;
    @Column(name = "game_matrix", updatable = true, insertable = true)
    private String gameState;
    @Column(name = "player1", updatable = false, insertable = true)
    private long player1;
    @Column(name = "player2", updatable = false, insertable = true)
    private long player2;
    @Column(name = "last_played_by", updatable = true, insertable = true)
    private long lastMoveBy;
    @Column(name = "saved_on", insertable = true, updatable = true, columnDefinition = "timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'")
    private Date updatedOn;
}
