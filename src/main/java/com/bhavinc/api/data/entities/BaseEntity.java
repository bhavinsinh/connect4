package com.bhavinc.api.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.proxy.HibernateProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by bhavi on 06-06-2016.
 */

@MappedSuperclass
@JsonIgnoreProperties(value = {"tableName"})
public abstract class BaseEntity<ID extends Serializable> implements Serializable {

    private static Logger log = LoggerFactory.getLogger(BaseEntity.class);


    private static final long serialVersionUID = 1L;
    public abstract ID getId();
    public abstract void setId(ID id);


    public final String getTableName(){
        Table table = getClass().getAnnotation(Table.class);
        if(table == null && (this instanceof HibernateProxy)){
            table = ((HibernateProxy)this).getHibernateLazyInitializer().getImplementation().getClass().getAnnotation(Table.class);
        }
        return table != null ? table.name() : "";
    }

    @Override
    public int hashCode() {
        if(getId()==null) return nullIdHashCode();
        return getId().hashCode();
    }

    protected int nullIdHashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseEntity<?> other = (BaseEntity<?>) obj;
        if (getId() == null) {
            return nullIdEquals(other);
        } else if (!getId().equals(other.getId())){
            return false;
        }
        return true;
    }


    protected boolean nullIdEquals(BaseEntity<?> other) {
        return false;
    }

}
