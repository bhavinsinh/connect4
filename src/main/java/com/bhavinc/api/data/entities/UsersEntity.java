package com.bhavinc.api.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bhavi on 05-06-2016.
 */
@Entity
@Table(name ="users")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class UsersEntity extends BaseEntity<Long> {
    @Column(name = "id", updatable = false, insertable = true)
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @Column(name = "username", insertable = true, updatable = true)
    private String username;
    @JsonIgnore
    @Column(name = "password", updatable = true, insertable = true)
    private String password;
    @Column(name = "name", updatable = true, insertable = true)
    private String userNiceName;
    @Column(name = "email", updatable = true, insertable = true)
    private String email;
    @Column(name = "mobile", updatable = true, insertable = true)
    private String mobile;
    @Column(name = "state", insertable = true, updatable = true)
    @Enumerated(value = EnumType.STRING)
    private UserState userState;
    @Column(name = "created_on", nullable = false, insertable = true, updatable = false)
    private Date created_on;
    @Column(name = "updated_on", insertable = false, updatable = true)
    private Date updated_on;
}
