package com.bhavinc.api.data.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by bhavi on 05-06-2016.
 */
@Entity
@Table(name ="games")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class GamesEntity extends BaseEntity<Long> {
    @Column(name = "id", updatable = false, insertable = true)
    @Id @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @Column(name = "state", insertable = true, updatable = true)
    @Enumerated(EnumType.STRING)
    private GameStateEnum state = GameStateEnum.inactive;
    @Column(name = "started_by", updatable = false, insertable = true)
    private long challenger;
    @Column(name = "challenged_to", updatable = true, insertable = true)
    private long challenged;
    @Column(name = "winner", updatable = true, insertable = true)
    private long winner;
    @Column(name = "created_on", nullable = false, insertable = true, updatable = false)
    private Date created_on;
    @Column(name = "updated_on", insertable = false, updatable = true, columnDefinition = "timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'")
    private Date updated_on;

}
