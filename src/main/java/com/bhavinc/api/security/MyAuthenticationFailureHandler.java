package com.bhavinc.api.security;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.bhavinc.api.data.model.bo.ErrorInfo;
import com.bhavinc.api.utils.Utility;

@Component
public class MyAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {



    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
        //super.onAuthenticationFailure(request, response, exception);
        Map<String,Object> mapResponse = new HashMap<>();
        mapResponse.put("statusCode",ErrorInfo.INVALID_SESSION_CODE);
        mapResponse.put("tid",request.getHeader("tid"));
        mapResponse.put("data",null);
        mapResponse.put("statusMessage",ErrorInfo.INVALID_SESSION_MESSAGE);
        response.setContentType("application/json");
        response.getWriter().write(Utility.jsonEncode(mapResponse));
        response.getWriter().flush();
    }

}