package com.bhavinc.api.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import lombok.Getter;
import lombok.Setter;

public class ServiceAuthentication implements Authentication {
	
	@Getter private final boolean authenticated;
	@Getter private final Object credentials;
	@Getter private final String serviceName;
	@Getter @Setter private Object details;
	
	public ServiceAuthentication(String serviceName, Object credentials) {
		this.serviceName = serviceName;
		this.credentials = credentials;
		this.authenticated = true;
	}

	@Override
	public String getName() {
		return serviceName;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.NO_AUTHORITIES;
	}

	@Override
	public Object getPrincipal() {
		return serviceName;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated)
			throws IllegalArgumentException {
		if(!isAuthenticated) throw new IllegalArgumentException();
	}

}
