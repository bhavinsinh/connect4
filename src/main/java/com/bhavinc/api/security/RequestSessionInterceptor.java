package com.bhavinc.api.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.bhavinc.api.data.model.dao.TokenUtils;
import com.bhavinc.api.utils.Utility;
import com.bhavinc.core.logging.ControllerLogger;


public class RequestSessionInterceptor implements HandlerInterceptor {

    @Autowired
    private TokenUtils tokenUtils;
    private final static String tidHeader = "tid";


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        SessionInfo sessionData = (SessionInfo) request.getAttribute("sessionData");
        String tid = request.getHeader(tidHeader);
        if(sessionData == null) {
            String sessionInfoString = tokenUtils.getSession(tid);
            if (sessionInfoString == null || sessionInfoString.equals("{}")) {
                String token = tokenUtils.getToken("{}");
                tid = token;
                sessionData = new SessionInfo();
                sessionData.setId(token);
            } else {
                sessionData = (SessionInfo) Utility.jsonDecode(sessionInfoString, SessionInfo.class);
            }
        }

        request.setAttribute("sessionData",sessionData);
        request.setAttribute("logs", null);
        request.setAttribute("tid", tid);
        MDC.put(tidHeader, tid);
        String user_agent = request.getHeader("user-agent");
        request.setAttribute("user_agent",user_agent);
        sessionData.setUpdate(false);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        ControllerLogger.getLogger().get().clear();
    }

    /**
     * THis is called after user response is rendered
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        SessionInfo sessionData = (SessionInfo)request.getAttribute("sessionData");
        tokenUtils.update(sessionData.getId(), sessionData);
    }

}
