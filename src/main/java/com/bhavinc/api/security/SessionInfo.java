package com.bhavinc.api.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;
import com.bhavinc.api.pojo.SignUp;
import com.bhavinc.api.pojo.User;


import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter 
@Setter
public class SessionInfo   {
	//TODO:: convert SessionInfo class to map 

	private String id;

	private boolean update;
	
	@JsonProperty("user") 
	User user;

	@JsonProperty("signup_data") 
	@JsonSerialize(include = Inclusion.ALWAYS)
	SignUp signupData;


    public SessionInfo(){}

    //@JsonCreator
    public SessionInfo( String id, boolean update, User user) {
        this.id = id;
        this.update = update;
        this.user = user;
    }

    public SessionInfo(String otp,String mobile,String name,String email){
        User newUser = new User();
        newUser.setOtp(otp);
        newUser.setMobile(mobile);
        newUser.setName(name);
        newUser.setEmail(email);
        this.user = newUser;
        this.update = false;
    }
}
