package com.bhavinc.api.security;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.google.common.base.Preconditions;

public class BasicAuthInternalServiceAuthenticationProvider implements
		AuthenticationProvider {

	private static final String SwiggyUsername = "GGYSWI";
	private static final String SwiggyPassword = "2015SW!GGY";

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		Preconditions.checkArgument(UsernamePasswordAuthenticationToken.class.isInstance(authentication),
				"Auth provider passed an authentication object that was not a UsernamePasswordAuthenticationToken");

		// Determine username
		String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED"
				: authentication.getName();
		if(!SwiggyUsername.equals(username)) {
			throw new UsernameNotFoundException("Username is not valid - " + username);
		}

		if(null == authentication.getCredentials()) {
			throw new AuthenticationCredentialsNotFoundException("No password provided.");
		}
		String presentedPassword = authentication.getCredentials().toString();
		if(!SwiggyPassword.equals(presentedPassword)) {
			throw new BadCredentialsException("Incorrect password");
		}

		ServiceAuthentication authenticated = new ServiceAuthentication(SwiggyUsername, authentication.getCredentials());
		authenticated.setDetails(authentication.getDetails());

		return authenticated;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication));
	}

}
