package com.bhavinc.api.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.data.model.dao.TokenUtils;
import com.bhavinc.api.errors.BaseError;
import com.bhavinc.api.pojo.ErrorResponse;
import com.bhavinc.api.utils.Utility;

public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    @Autowired TokenUtils tokenUtils;

    protected TokenAuthenticationFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl); //defaultFilterProcessesUrl - specified in applicationContext.xml.
        super.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(defaultFilterProcessesUrl)); //Authentication will only be initiated for the request url matching this pattern
        setAuthenticationManager(new NoOpAuthenticationManager());
        setAuthenticationSuccessHandler(new TokenSimpleUrlAuthenticationSuccessHandler());
        setAuthenticationFailureHandler(new MyAuthenticationFailureHandler());
    }

    /**
     * Attempt to authenticate request
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException,
            IOException, ServletException {
        String tid = request.getHeader("tid");
        logger.info("tid found:"+ tid);
        AbstractAuthenticationToken userAuthenticationToken = authUserByToken(tid, request);
        if(userAuthenticationToken == null) throw new AuthenticationServiceException("Invalid Token");
        return userAuthenticationToken;
    }

    /**
     * authenticate the user based on token
     * @return
     */
    private AbstractAuthenticationToken authUserByToken(String tid, HttpServletRequest request) throws JsonProcessingException {
        if(tid == null) return null;

        AbstractAuthenticationToken authToken =null;

        Map<String,Object> res = tokenUtils.validate(tid, true, request);
        if((Boolean)res.get("isValid")) {
            SessionInfo sessionData = (SessionInfo) res.get("sessionData");
            request.setAttribute("sessionData",sessionData);
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

            User principal = new User(tid, sessionData.getUser().getToken(), authorities);
            authToken = new UsernamePasswordAuthenticationToken(principal, "", principal.getAuthorities());
        }
        else{
            BaseError error = (BaseError) res.get("error");
            ErrorResponse response = new ErrorResponse(error,null);
            String message = Utility.jsonEncode(response);
            throw new AuthenticationServiceException(message);
        }

        return authToken;
    }


    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        super.doFilter(req, res, chain);
    }
}
