package com.bhavinc.api.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import com.bhavinc.api.pojo.ErrorResponse;
import com.bhavinc.api.utils.Utility;


public class CustomAuthenticationEntryPoint extends
        BasicAuthenticationEntryPoint {

    Logger logger = LoggerFactory.getLogger(CustomAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {

        logger.info("Unauthorized request." + authException.getMessage());

        response.setStatus(HttpStatus.SC_UNAUTHORIZED);
        response.getWriter().write(Utility.jsonEncode(new ErrorResponse(HttpStatus.SC_UNAUTHORIZED, "Invalid Credentials")));


    }
}
