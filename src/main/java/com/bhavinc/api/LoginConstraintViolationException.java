package com.bhavinc.api;


public class LoginConstraintViolationException extends Exception {
    public LoginConstraintViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
