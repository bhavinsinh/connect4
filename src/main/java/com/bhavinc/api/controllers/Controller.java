package com.bhavinc.api.controllers;

import com.bhavinc.api.security.SessionInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.pojo.BaseResponse;
import com.bhavinc.api.pojo.GameInput;
import com.bhavinc.api.pojo.GameMove;
import com.bhavinc.api.services.GameServices.GameService;
import com.bhavinc.api.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by bhavi on 03-06-2016.
 */
@RestController
@RequestMapping("v1/play/")
public class Controller {

    @Autowired
    GameService gameService;

    @RequestMapping("/test")
    public String testEndPoint(HttpServletRequest request,@RequestParam String test) {
        return test + "success";
    }

    @RequestMapping(value = "/user/all",
            method = RequestMethod.GET, produces = "application/json")
    public BaseResponse getActiveUsers(HttpServletRequest request, @RequestParam Boolean active) {
        return gameService.getUsers(active);
    }

    @RequestMapping(value = "/user/status",
            method = RequestMethod.GET, produces = "application/json")
    public BaseResponse getActiveUsers(HttpServletRequest request) {
        SessionInfo sessionInfo = (SessionInfo) request.getAttribute("sessionData");
        return gameService.getUserStatus(sessionInfo.getUser().getUserId());
    }

    @RequestMapping(value = "/game/state",
            method = RequestMethod.GET, produces = "application/json")
    public BaseResponse getCurrentGameState(HttpServletRequest request, @RequestParam Long gameID) {
        return gameService.getCurrentGameState(gameID);
    }

    @RequestMapping(value = "/game/play",
            method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public BaseResponse gameMove(HttpServletRequest request, @RequestBody GameMove gameMove) {
        try {
            System.out.println(Utility.jsonEncode(gameMove));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        SessionInfo sessionInfo = (SessionInfo) request.getAttribute("sessionData");
        gameMove.setUserID(sessionInfo.getUser().getUserId());
        return gameService.playMove(gameMove, sessionInfo);
    }


    @RequestMapping(value = "/create",
            method = RequestMethod.POST, consumes = "application/json")
    public BaseResponse createGame(HttpServletRequest request, @RequestBody GameInput gameInput) {
        //Don't use client sent gameInput userId. But only userId against TID.
        SessionInfo sessionInfo = (SessionInfo) request.getAttribute("sessionData");
        gameInput.setChallengerId(sessionInfo.getUser().getUserId());
        return gameService.createGame(gameInput);
    }

    @RequestMapping(value = "/accept",
            method = RequestMethod.POST, consumes = "application/json")
    public BaseResponse acceptGame(HttpServletRequest request, @RequestBody GameInput gameInput) throws JsonProcessingException {
        //Don't use client sent gameInput userId. But only userId against TID.
        SessionInfo sessionInfo = (SessionInfo) request.getAttribute("sessionData");
        gameInput.setChallengedId(sessionInfo.getUser().getUserId());
        return gameService.accept(gameInput);
    }
}
