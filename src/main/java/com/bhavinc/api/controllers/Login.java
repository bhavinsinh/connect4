package com.bhavinc.api.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.LoginConstraintViolationException;
import com.bhavinc.api.data.model.bo.ErrorInfo;
import com.bhavinc.api.errors.BaseError;
import com.bhavinc.api.input.LoginData;
import com.bhavinc.api.pojo.BaseResponse;
import com.bhavinc.api.pojo.ErrorResponse;
import com.bhavinc.api.pojo.SignUp;
import com.bhavinc.api.pojo.SuccessResponse;
import com.bhavinc.api.security.SessionInfo;
import com.bhavinc.api.services.GameServices.UserService;
import com.bhavinc.core.exceptions.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.bhavinc.api.utils.Utility.isValidEmail;
import static com.bhavinc.api.utils.Utility.isValidMobileNumber;
import static com.bhavinc.api.utils.Utility.jsonEncode;

@RestController
@RequestMapping("v1/play/auth")
class Login {
    private static final Logger logger = LoggerFactory
            .getLogger(Login.class);

    @Autowired
    UserService userservice;

    @RequestMapping(value = "/signup",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse signup(@RequestBody SignUp newSignup,
                               @RequestHeader(value = "user-agent", required = false) String userAgent,
                               HttpServletRequest request
    ) throws BadRequestException, JsonProcessingException {

        SessionInfo sInfo = (SessionInfo) request.getAttribute("sessionData");
        if (newSignup == null || newSignup.getName() == null)
            return new ErrorResponse("empty name", null);
        if (!isValidMobileNumber(newSignup.getMobile()))
            return new ErrorResponse("invalid mobile number", null);
        if (!isValidEmail(newSignup.getEmail()))
            return new ErrorResponse("invalid email", null);

        if (newSignup.getPassword().length() < 6)
            return new ErrorResponse("Password length is too short", null);

        Map<String, String> headers = new HashMap<String, String>() {{
            put("user_agent", userAgent);
        }};
        newSignup.setHeaders(headers);
        logger.debug("Headers For Signup:" + jsonEncode(headers));
        Map<String, Object> result = userservice.newSignup(newSignup, sInfo);
        if (result.containsKey(ErrorInfo.errorCode)) {
            BaseError e = (BaseError) result.get(ErrorInfo.errorCode);
            return new ErrorResponse(e);
        }
        SuccessResponse responseBody = new SuccessResponse(result);
        return responseBody;
    }

    @RequestMapping(
            value = "/login",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )

    public BaseResponse loginViaPassword(@RequestBody LoginData loginData, HttpServletRequest request) throws IOException, LoginConstraintViolationException {
        SessionInfo sInfo = (SessionInfo)request.getAttribute("sessionData");
        return userservice.loginViaPassword(loginData,sInfo);
    }


}