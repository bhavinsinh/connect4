package com.bhavinc.api.pojo;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.bhavinc.api.utils.Utility;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SignUp {


    @Getter @Setter private String name;
    @Getter @Setter private String mobile;
    @Getter @Setter private String email;
    @Getter @Setter private String password;
    @Getter @Setter private Map<String,String> headers;
    @Getter @Setter private String otp;

    @Override
    public String toString(){
        try {
            return Utility.jsonEncode(this);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

}
