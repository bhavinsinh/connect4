package com.bhavinc.api.pojo;


public class SuccessResponse extends BaseResponse  {

    public SuccessResponse(Object data){
        super(0, "done successfully", data);
    }

    public SuccessResponse(String status,Object data){
        super(0, status, data);
    }

    public SuccessResponse() {
        super(0, "done successfully", null);
    }

    public SuccessResponse(String message) {
        super(0, message, null);
    }
}
