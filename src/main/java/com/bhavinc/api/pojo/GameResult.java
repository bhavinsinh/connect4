package com.bhavinc.api.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by bhavi on 08-06-2016.
 */
@JsonSerialize
@Getter
@Setter
public class GameResult {
    boolean gameOver = false;
    Long winner;
}
