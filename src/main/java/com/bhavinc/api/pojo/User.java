package com.bhavinc.api.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import com.bhavinc.api.data.entities.UsersEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class User {

	private String otp;
	private boolean verified = false;
	private String mobile;
	private String email;
	private int attempt;
    private long gameId;

	@JsonProperty("token") 
	private String token;
	
	@JsonProperty("user_id") @JsonSerialize(using=ToStringSerializer.class)
	private long userId;
	
	@JsonProperty("name") 
	private String name;

    public User(String otp){
        this.otp = otp;
    }

    public User(){}

    /**
     * Dummy user for signup.
     * @param signUp
     */
    public User(SignUp signUp){
        otp = signUp.getOtp();
        mobile = signUp.getMobile();
        email = signUp.getEmail();
        name = signUp.getName();
        userId = -1;
    }
    public User(String otp, UsersEntity user){
        this.otp = otp;
        mobile= user.getMobile();
        email = user.getEmail();
        name = user.getUserNiceName();
        userId = user.getId();
    }
    private boolean emailVerified = false;
    private boolean mobileUpdateVerified = false;
}
