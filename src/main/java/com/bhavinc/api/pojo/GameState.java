package com.bhavinc.api.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by bhavi on 05-06-2016.
 */
@Getter
@Setter
@JsonSerialize
public class GameState {
    long[][] matrix = new long[6][7];
}
