package com.bhavinc.api.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by bhavi on 06-06-2016.
 */
@Data
@JsonSerialize
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class GameInput {
    Long gameId;
    Long challengerId;
    Long challengedId;
}
