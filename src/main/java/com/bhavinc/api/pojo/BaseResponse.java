package com.bhavinc.api.pojo;

import com.bhavinc.api.utils.Utility;

import lombok.*;

//@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse {

    private int statusCode;
    private String statusMessage;
    private Object data;
    private String tid;
    private String deviceId;

    private static final String TID = "tid";

    public BaseResponse(int statusCode, String statusMessage, Object data) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.data = data;
        String headerTid = (String) Utility.getCurrentRequest().getAttribute(TID);
        this.tid = headerTid;
    }


    public BaseResponse(int statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    /**
     * Copies tid from request header and updates it.
     *
     * @return
     */
    public void updateTid(String token) {
        tid = token;
    }


}
