package com.bhavinc.api.pojo;

public enum Day {
    MON(1,"mon"),TUE(2,"tue"),WED(3,"wed"),THU(4,"thu"),FRI(5,"fri"),SAT(6,"sat"),SUN(7,"sun");

    private int day;
    private String name;

    Day(int day,String name){this.day = day;this.name = name;}

    public int getDay(){
        return day;
    }

    public String getName(){
        return name;
    }

    public static Day getDay(int i){
        for(Day d : Day.values())
            if(d.getDay() == i)
                return d;
        return null;
    }

    public static  Day getDay(String s){
        for(Day d : Day.values())
            if(s.toLowerCase().equals(d.getName()))
                return d;
        return null;
    }
}
