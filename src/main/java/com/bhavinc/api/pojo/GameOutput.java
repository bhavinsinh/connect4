package com.bhavinc.api.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

/**
 * Created by bhavi on 06-06-2016.
 */
@JsonSerialize
@Data
public class GameOutput {
    Long gameId;
}
