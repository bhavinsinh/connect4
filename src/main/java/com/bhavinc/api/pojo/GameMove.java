package com.bhavinc.api.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by bhavi on 08-06-2016.
 */
@JsonSerialize
@Getter
@Setter
public class GameMove {
    int row;
    int col;
    Long gameId;
    @JsonIgnore
    Long userID;
}
