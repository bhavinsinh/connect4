This provides a backend for a two player game Connect4 (Can be easily adapted for any two player turn based game). 
The flow of the game is that in an arena all logged in and free users are shown. 
A player can challenge a user and the other user can accept 
the challenge and game would be in active mode. After that users take turn to make their move. 
If game is over backend returns status codewith winner. 
Long polling needs to be used to get user status (if game challenge is made) and also for change in game state. 

Stack: Java/Spring/Hibernate
Database:MySql
Cache/SessionStorage : Redis
Security: TokenBasedAuthentication



